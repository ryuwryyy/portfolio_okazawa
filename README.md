# Portforio
## Overview
1. data_science: Python/画像認識/機械学習/統計(専攻:多変量解析)(経験1年半)
2. Web Engineering: Ruby on Rails/AWS(経験3ヶ月)
3. HP制作: Wordpress/css/JS/AdobePs/Ai/Pr(経験半年)
4. Unity: C#/CakePHP(経験：1ヶ月)

## - spocom
スポーツを動画で楽しく学ぶWebアプリ　J1サッカークラブでの検証実験採用

## - 2DVtuber
リアルタイムトラッキングによる２Dアバターの動画配信環境（通称 Vtuber）
### 使用技術
Unity/C#/2Dmodel

## - crossityEnglishApp
気軽に異文化交流できる掲示板・チャット機能のWebアプリケーション 県内の100人の外国人が利用
### 使用技術
Ruby on Rails/AWS/Javascript

## - m-gram_analysis
400万人の性格データを統計分析 簡易レポート 個人情報保護により一部非公開
### 使用技術
Python/多変量解析（統計）/DeepLearning(CNN)

## - anomaly_detection_string
太陽光パネルの異常検知　理工学部の教授のデータ分析サポート 研究データにつき一部非公開
### 使用技術
Python/K-NN/データ前処理

## - card_image_detector
クレジットカードの種類を識別する画像認識AI 業務委託契約により一部非公開
### 使用技術
Python/TensorFlow/Docker/AWS/データ前処理/WebScraping
